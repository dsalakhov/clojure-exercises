(ns quipper.core
  (require [clojure.tools.cli :as cli]
           [clojure.java.io :as io])
  (:gen-class))

(def cli-options [["-h" "--help" "Print this help info"
                   :default false :flag true]
                  ["-f" "--file FILE" "Specify an alternate quip file."
                   :default (str
                              (System/getProperty "user.home")
                              "/.quips")]])

(defn print-help []
  (println "This is the 'quipper' program."))

(defn read-quips
  "Returns vector of quips or nil if file doesn't exist"
  [file]
  (when (.exists (io/file file))
    (with-open [rdr (io/reader file)]
      (mapv read-string (line-seq rdr)))))

(defn- write-quips
  "Writes collection of quips to the file"
  [file quips]
  (with-open [wrt (io/writer file :append true)]
    (doseq [line quips]
      (.write wrt (prn-str line)))))

(defn add-quips [file quips]
  (if (seq quips)
    (write-quips file quips)
    (print-help)))

(defn get-quip [file]
  (when-let [quips (seq (read-quips file))]
    (print (rand-nth quips))))

(defn count-quips [file]
  (print (count (read-quips file))))

(defn drop-quips [file]
  (io/delete-file file))

(defn cmd
  [command file quips]
  (case (keyword command)
    :add (add-quips file quips)
    :count (count-quips file)
    :drop (drop-quips file)
    (get-quip file)))

(defn -main
  "Deal with quips."
  [& args]
  (let [{{:keys [help file]} :options arguments :arguments}(cli/parse-opts args cli-options)
        [command & quips] arguments]
    (if help
      (print-help)
      (cmd command file quips))))