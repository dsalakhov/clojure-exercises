(ns quipper.core-test
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [midje.sweet :refer :all]
            [quipper.core :refer :all]))

(def test-file "quipper_test_file.txt")

(defn delete-test-file
  "Delete the test file and swallow the exception if the file doesn't exist"
  []
  (try (io/delete-file test-file)
       (catch Exception _)))

(facts "Checks on the 'main' function"
       (with-state-changes [(before :facts (delete-test-file))
                            (after :facts (delete-test-file))
                            ]
                           (let [banner "This is the 'quipper' program."
                                 quip   "If at first you don’t succeed... so much for skydiving."
                                 mquip  (str "First the doctor told me the good news: \n"
                                             "I was going to have a disease named after me.")]
                             (letfn [(test-main [args] (->> args
                                                            (apply -main "-f" test-file)
                                                            with-out-str
                                                            str/trim-newline))
                                     (get-quip [] (test-main []))
                                     (drop-quip [] (test-main ["drop"]))
                                     (count-quip [] (test-main ["count"]))
                                     (add-quip [quips] (->> quips
                                                            (concat ["add"])
                                                            test-main))
                                     (get-raw-quip [] (try (slurp test-file) (catch Exception _)))]

                               (fact "Should get a help banner when main is called with '-h'"
                                     (with-out-str (-main "-h")) => (contains banner))
                               (fact "Should get a help banner when trying to 'add' without a quip"
                                     (add-quip []) => (contains banner))
                               (fact "Should be able to add a quip"
                                     (add-quip [quip]) => str/blank?
                                     (get-raw-quip) => (contains quip)
                               )
                               (fact "Should be able to get the quip"
                                     (add-quip [quip]) => str/blank?
                                     (get-quip) => quip)
                               (fact "Should be able to drop all the existing quips with a 'drop'"
                                     (add-quip [quip]) => str/blank?
                                     (drop-quip) => str/blank?
                                     (get-quip) => str/blank?)
                               (fact "Should be able to add a multiline quip"
                                     (add-quip [mquip]) => str/blank?
                                     (get-quip) => mquip)
                               (fact "Should be able to add multiple quips"
                                     (add-quip [quip mquip]) => str/blank?
                                     (get-raw-quip) => (and (contains mquip) (contains quip)))
                               (fact "Should be able to get all quips in store"
                                     (let [quips #{quip mquip}]
                                       (add-quip quips) => str/blank?
                                       (loop [seen-quips #{} counter 1000] ;; Don't want this to go forever
                                         (if (or (zero? counter) (= quips seen-quips))
                                           seen-quips
                                           (recur (conj seen-quips (get-quip)) (dec counter))))
                                       => quips))
                               (fact "Should be able to get a quip count"
                                     (add-quip [quip mquip "The third quip"]) => str/blank?
                                     (count-quip) => "3")
                               ))))